import Card from "@/components/Card/Card"
import Link from "next/link"

type Props = {
  id: string
  name: string
  description: string
  photo: {
    url: string
    title: string
  }
}

const DishCard = (p: Props) => {
  return (
    <Card className="p-0 h-28 mb-4">
      <Link key={p.name} href={`/dishes/${p.id}`} className="flex h-full gap-4">
        <div className="flex h-full w-32 shrink-0">
          <img
            src={p.photo.url}
            alt={p.photo.title}
            className="h-full w-auto object-cover grow"
          />
        </div>

        <div className="pt-2 pr-2">
          <h2 className="">{p.name}</h2>
          <div className="">{p.description}</div>
        </div>
      </Link>
    </Card>
  )
}

export default DishCard

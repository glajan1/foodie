import { twMerge } from "tailwind-merge"

interface Props {
  children: React.ReactNode
  className?: string
}

export default function Card({ children, className }: Props) {
  return (
    <div
      className={twMerge(
        `flex flex-col grow bg-slate-100 shadow-2xl rounded-lg overflow-hidden p-6 h-full ${className}`
      )}
    >
      {children}
    </div>
  )
}

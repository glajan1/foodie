# About
This is a project to try out some, for me, new tech.
It is a simple recipe database and website. 

The tech used is:
* TypeScript
* React
* NextJS
* Tailwind CSS
* Contentful (Headless CMS)
* graphQL
* Vercel

Of which Contentful and Tailwind is new to me.

## Getting Started

Run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.


## Production
The app is deployed to Vercel on this url:


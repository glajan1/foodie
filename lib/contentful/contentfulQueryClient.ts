import { CONTENTFUL_API_URL } from "@/app/urls"

export const contentful_query = async (gqlQuery: any, variables?: any) => {
  const res = await fetch(CONTENTFUL_API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${process.env.CONTENTFUL_ACCESS_TOKEN}`,
    },
    body: JSON.stringify({
      query: gqlQuery,
      variables,
    }),
  })
  const json = await res.json()
  return json.data
}

import { gql } from "graphql-request"
import { contentful_query } from "./contentfulQueryClient"

export const query_all_dishes = async (args?: { limit?: number }) => {
  const allDishesQuery = gql`
    query allDishes($limit: Int!) {
      dishCollection(limit: $limit) {
        items {
          name
          description
          ingredients
          photo {
            title
            url
          }
          sys {
            id
          }
        }
      }
    }
  `
  return await contentful_query(allDishesQuery, { ...args })
}

export const query_one_dish = async (args: { id: string }) => {
  const oneDishQuery = gql`
    query oneDish($id: String!) {
      dish(id: $id) {
        name
        description
        ingredients
        cookingInstructions {
          json
        }
        isVegetarian
        photo {
          title
          width
          height
          url
        }
        sys {
          id
        }
      }
    }
  `
  return await contentful_query(oneDishQuery, { ...args })
}

export const query_footer = async () => {
  const footerQuery = gql`
    query footer {
      footerCollection {
        items {
          main
        }
      }
    }
  `
  return await contentful_query(footerQuery)
}

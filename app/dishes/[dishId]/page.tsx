import Card from "@/components/Card/Card"
import { query_one_dish } from "@/lib/contentful/contentfulQueries"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"

export default async function Dish({ params }: { params: { dishId: string } }) {
  const res = await query_one_dish({ id: params.dishId })
  const dish = res.dish

  return (
    <main
      className="main grid grid-cols-1 sm:grid-cols-2 gap-5 bg-cover bg-center bg-no-repeat bg-blend-luminosity bg-green-200"
      style={{ backgroundImage: `url(${dish?.photo?.url})` }}
    >
      <h1 className="flex text-center justify-center col-span-full bg-slate-100 bg-opacity-75 p-3">{`${dish.name}`}</h1>

      <Card className="max-h-96 p-1">
        <img
          className="object-center object-cover w-full h-full rounded-[inherit]"
          src={dish?.photo?.url}
          alt={dish?.photo?.title}
        />
      </Card>

      <Card className="h-auto">
        <div>{`${dish.description}`}</div>
      </Card>

      <Card>
        <h2>Ingredienser</h2>
        <pre className="whitespace-pre-wrap">{`${dish.ingredients}`}</pre>
      </Card>

      <Card>
        <h2>Tillagning</h2>
        <div>{documentToReactComponents(dish.cookingInstructions.json)}</div>
      </Card>
    </main>
  )
}

import DishCard from "@/components/DishCard/DishCard"
import { query_all_dishes } from "@/lib/contentful/contentfulQueries"

export default async function Home() {
  const res = await query_all_dishes({ limit: 0 })

  return (
    <main className="main">
      <div>
        <h1>Dishes</h1>
        <div>
          {res.dishCollection.items.map((dish: any) => {
            const dishCardProps = {
              id: dish.sys.id,
              name: dish.name,
              description: dish.description,
              photo: dish.photo,
            }
            return (
              <DishCard key={dishCardProps.id} {...dishCardProps}></DishCard>
            )
          })}
        </div>
      </div>
    </main>
  )
}

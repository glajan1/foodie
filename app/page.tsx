import DishCard from "@/components/DishCard/DishCard"
import { query_all_dishes } from "@/lib/contentful/contentfulQueries"

export default async function Home() {
  const dishes = await query_all_dishes({ limit: 3 })

  return (
    <main className="main">
      <div>
        <h1>Senaste rätterna</h1>
        <div>
          {dishes.dishCollection.items.map((dish: any) => {
            const dishCardProps = {
              id: dish.sys.id,
              name: dish.name,
              description: dish.description,
              photo: dish.photo,
            }
            return (
              <DishCard key={dishCardProps.id} {...dishCardProps}></DishCard>
            )
          })}
        </div>
      </div>
    </main>
  )
}
